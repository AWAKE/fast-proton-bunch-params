using ImageFiltering # Use convolution filter to find approximate bunch centroid

function get_event_data(path)
    
    fid = h5open(path, "r")
    
    img_50 = []
    try 
        x_ticks_50 = read(fid["/AwakeEventData/TT41.BTV.412350/Image/imagePositionSet1"])[:,1]
        y_ticks_50 = read(fid["/AwakeEventData/TT41.BTV.412350/Image/imagePositionSet2"])[:,1]
        img_50 = reshape(read(fid["/AwakeEventData/TT41.BTV.412350/Image/imageSet"])[:,1], length(x_ticks_50), length(y_ticks_50))
    catch 
        img_50 = NaN
    end
    
    img_53 = []
    try 
        x_ticks_53 = read(fid["/AwakeEventData/TT41.BTV.412353/Image/imagePositionSet1"])[:,1]
        y_ticks_53 = read(fid["/AwakeEventData/TT41.BTV.412353/Image/imagePositionSet2"])[:,1]
        img_53 = reshape(read(fid["/AwakeEventData/TT41.BTV.412353/Image/imageSet"])[:,1], length(x_ticks_53), length(y_ticks_53))
    catch
        img_53 = NaN 
    end
    
    img_54 = []
    try 
        img_54 = read(fid["/AwakeEventData/BOVWA.11TCC4.AWAKECAM11/ExtractionImage/imageRawData"])
    catch
        img_54 = NaN 
    end
    
    img_26 = []
    try 
        x_ticks_26 = read(fid["/AwakeEventData/TT41.BTV.412426/Image/imagePositionSet1"])[:,1]
        y_ticks_26 = read(fid["/AwakeEventData/TT41.BTV.412426/Image/imagePositionSet2"])[:,1]
        img_26 = reshape(read(fid["/AwakeEventData/TT41.BTV.412426/Image/imageSet"])[:,1], length(x_ticks_26), length(y_ticks_26));
    catch
        img_26 = NaN 
    end
    
    img_is1_core = []
    try 
        img_is1_core = read(fid["/AwakeEventData/BOVWA.07TCC4.AWAKECAM07/ExtractionImage/imageRawData"])
    catch
        img_is1_core = NaN 
    end
    
    img_is1_halo = []
    try 
        img_is1_halo = read(fid["/AwakeEventData/BOVWA.06TCC4.AWAKECAM06/ExtractionImage/imageRawData"])
        img_is1_halo = img_is1_halo[end:-1:1,end:-1:1] 
    catch
        img_is1_halo = NaN 
    end
    
    img_is2_core = []
    try 
        img_is2_core = read(fid["/AwakeEventData/BOVWA.02TCC4.AWAKECAM02/ExtractionImage/imageRawData"])
        img_is2_core = img_is2_core' # needs to be rotated
        img_is2_core = img_is2_core[end:-1:1,:]
    catch
        img_is2_core = NaN 
    end
    
    img_is2_halo = []
    try 
        img_is2_halo = read(fid["/AwakeEventData/BOVWA.04TCC4.AWAKECAM04/ExtractionImage/imageRawData"])
    catch
        img_is2_halo = NaN 
    end
    
    close(fid)
    
    return (cam_1=img_50, cam_2=img_53, cam_3=img_54, cam_4=img_26, cam_5=img_is1_core, cam_6=img_is1_halo, cam_7=img_is2_core, cam_8=img_is2_halo)
end

function plot_event_data(edata)
    
    fig, ax = plt.subplots(2,4, figsize=(8,6))
    
    fig.subplots_adjust(hspace=0.3, wspace=0.3)

    plot_cam(edata.cam_1, ax[1,1])
    ax[1,1].set_title("BTV50")

    plot_cam(edata.cam_2, ax[1,2])
    ax[1,2].set_title("BTV53")
    
    plot_cam(edata.cam_3, ax[1,3])
    ax[1,3].set_title("BTV54")

    plot_cam(edata.cam_4, ax[1,4])
    ax[1,4].set_title("BTV26")
    
    plot_cam(edata.cam_5, ax[2,1])
    ax[2,1].set_title("IS1 Core")

    plot_cam(edata.cam_6, ax[2,2])
    ax[2,2].set_title("IS1 Halo")
    plot_cam(edata.cam_7, ax[2,3])
    ax[2,3].set_title("IS2 Core")

    plot_cam(edata.cam_8, ax[2,4])
    ax[2,4].set_title("IS2 Halo")
end

function plot_cam(data, axis)
    
    xpr, ypr = get_marginals(data)
    
    axis.pcolormesh(data, vmin=0, vmax=2^12, rasterized=true, cmap="Spectral")

    ax2 = axis.twinx()
    ax3 = axis.twiny()

    ax2.fill_between(1:length(xpr), xpr, color="white", alpha=0.5)
    ax2.set_ylim(0, 4)

    ax3.fill_betweenx(1:length(ypr), ypr, color="white", alpha=0.5)
    ax3.set_xlim(0, 4)
    
    ax2.set_yticks([])
    ax3.set_xticks([])

    return axis
end


function def_projections(edata, cameras; det_maximum = find_max)
    
    nt_keys = collect(keys(edata))
    δ = [45, 45, 140, 40, 140, 140, 140, 140]
    
    projected_data = NamedTuple()
    
    for (ind, vals) in enumerate(nt_keys)
        data_cam = getindex(edata, vals)
        
        if ind in cameras
            δ_cam = δ[ind]
            
            xpr, ypr = get_marginals(data_cam)
            xmax, ymax = det_maximum(vals, xpr, ypr) #argmax(xpr), argmax(ypr)
            
            xstart, xstop = xmax - δ_cam, xmax + δ_cam
            ystart, ystop = ymax - δ_cam, ymax + δ_cam

            xpr, ypr = get_marginals(data_cam[ystart:ystop,xstart:xstop,])
            
            cam_tmp = (vals => (x=xpr, y=ypr),)
            projected_data = merge(projected_data, cam_tmp)
        else 
            cam_tmp = (vals => (x=NaN, y=NaN),)
            projected_data = merge(projected_data, cam_tmp)
        end
        
    end
    
    return projected_data
end

function get_marginals(data)
    
    xpr = sum(data,dims=1); xpr = xpr / maximum(xpr); xpr=[xpr...]
    ypr = sum(data,dims=2); ypr = ypr / maximum(ypr); ypr=[ypr...]
    
    return xpr, ypr
end

function conv_data(data; kernelsize=3)
    kern = KernelFactors.gaussian((kernelsize))
    cv_matrix = imfilter(data, kern)
end

function find_max(cam_ind, xpr, ypr)
    return 74+argmax(conv_data(xpr[75:end-100])), 74+argmax(conv_data(ypr)[75:end-100])
end

function const_max_def(vals)
    return constant_maximum(cam_ind, xpr, ypr; vals = vals ) = getindex(vals, cam_ind)
end

function find_2d_max(data)
    xpr, ypr = get_marginals(data)
    xmax, ymax = find_max(1.0, xpr, ypr)
    return xmax, ymax
end

function plot_crossections(dt, point) # plot_crossections(2d_image, find_2d_max(2d_image)) to plot crossection at maximum value
    xtmp, ytmp = point
    fig, ax = plt.subplots(1,3, figsize=(9,3))
    ax[1].pcolormesh(dt, cmap="Spectral")
    ax[1].axhline(ytmp, color=:lightgray, ls=:dotted)
    ax[1].axvline(xtmp, color=:lightgray, ls=:dotted)
    xproj = dt[ytmp,:]
    yproj = dt[:,xtmp]
    ax[2].fill_between(1:length(xproj), xproj, step="pre", color=:gray)
    ax[3].fill_between(1:length(yproj), yproj, step="pre", color=:gray)
    ax[2].set_ylim(bottom=0.0)
    ax[3].set_ylim(bottom=0.0)
    fig.suptitle("Cross Sections") 
end

function plot_projections(data; model_data=missing, savefig=false, savepath=[])
    
    nt_keys = collect(keys(data))
    cam_names = ["BTV50", "BTV53", "BTV54", "BTV26", "IS1 Core", "IS1 Halo", "IS2 Core", "IS2 Halo"]
    
    fig, ax = plt.subplots(2,8, figsize=(15,5), sharey=true)

    fig.subplots_adjust(hspace=0.0, wspace=0.1)
    
    ax[1].set_ylim(0, 1.1)

    for i in 1:8
        data_cam = getindex(data, nt_keys[i]) 
        
        range = 1:length(data_cam.x)
        ax[1,i].fill_between(range, data_cam.x, step="pre", color=:gray) 
        ax[2,i].fill_between(range, data_cam.y, step="pre", color=:gray)
        ax[1,i].set_title(cam_names[i])
        
        ax[1,i].set_xticks([])
        ax[1,i].set_ylim(bottom=0.0)
    end
    
    if typeof(model_data) != Missing
        for i in 1:8
            data_cam = getindex(model_data, nt_keys[i]) 
            range = 1:length(data_cam.x)
            ax[1,i].step(range, data_cam.x, color=:red) 
            ax[2,i].step(range, data_cam.y, color=:red)

        end
    end
    
    ax[1,1].set_ylabel("Projection X")
    ax[2,1].set_ylabel("Projection Y")
    
    fig.text(0.5, 0.01, "Pixels", ha="center", )
    
    if savefig
        fig.savefig(savepath, bbox_inches = "tight") 
        plt.close(fig)
    end
    
end


function likelihood_cam(
        params::NamedTuple, 
        image::NamedTuple,
        cam_ind::Integer;
        n_threads::Integer = Threads.nthreads()
    ) where {F <: AbstractFloat}
    
    VT = eltype(params.tr_size_core)
    tot_loglik = 0.0   
    
    δ_x::VT = params.psx[cam_ind] * 10^-3
    δ_y::VT = params.psy[cam_ind] * 10^-3
    
    @inbounds μ_x::VT  = params.algmx[cam_ind] * δ_x
    @inbounds μ_y::VT  = params.algmy[cam_ind] * δ_y
    
    @inbounds σ_x_core::VT = sqrt.(params.tr_size_core[1]^2 + 10^-4*params.ang_spr_core[1]^2*(params.waist_core[1] - params.s_cam[cam_ind])^2) 
    @inbounds σ_y_core::VT = sqrt.(params.tr_size_core[2]^2 + 10^-4*params.ang_spr_core[2]^2*(params.waist_core[1] - params.s_cam[cam_ind])^2) 
    
    σ_x_core_res::VT = sqrt(σ_x_core^2 + (params.resx[cam_ind]*δ_x)^2)
    σ_y_core_res::VT = sqrt(σ_y_core^2 + (params.resy[cam_ind]*δ_y)^2) 
    
    @inbounds σ_x_halo::VT = sqrt.(params.tr_size_halo[1]^2 + 10^-4*params.ang_spr_halo[1]^2*(params.waist_halo[1] - params.s_cam[cam_ind])^2) 
    @inbounds σ_y_halo::VT = sqrt.(params.tr_size_halo[2]^2 + 10^-4*params.ang_spr_halo[2]^2*(params.waist_halo[1] - params.s_cam[cam_ind])^2) 
    
    σ_x_halo_res::VT = sqrt(σ_x_halo^2 + (params.resx[cam_ind]*δ_x)^2)
    σ_y_halo_res::VT = sqrt(σ_y_halo^2 + (params.resy[cam_ind]*δ_y)^2) 
    
    dist_core_x = Normal(μ_x, σ_x_core_res)
    dist_core_y = Normal(μ_y, σ_y_core_res)
    
    dist_halo_x = Normal(μ_x, σ_x_halo_res)
    dist_halo_y = Normal(μ_y, σ_y_halo_res)
    
    x_edges = range(0, length = length(image.x)+1, step=δ_x) 
    y_edges = range(0, length = length(image.y)+1, step=δ_y) 
    
    z_x = (params.mixt_pow.* diff(cdf.(dist_core_x, x_edges))) .+ ((1-params.mixt_pow).*diff(cdf.(dist_halo_x, x_edges)))
    z_y = (params.mixt_pow.* diff(cdf.(dist_core_y, y_edges))) .+ ((1-params.mixt_pow).*diff(cdf.(dist_halo_y, y_edges)))
    
    for pix_ind in 1:length(image.x)
            
        pix_prediction = params.light_amp[cam_ind]*z_x[pix_ind]
#         tot_loglik += logpdf(Normal(pix_prediction + params.pedestal[cam_ind], params.light_fluct[cam_ind]*sqrt(pix_prediction) ), image.x[pix_ind]) 
        tot_loglik += logpdf(Normal(pix_prediction + params.pedestal[cam_ind], sqrt(params.light_fluct[cam_ind]^2*pix_prediction + (params.bckgr_fluct[cam_ind])^2) ), image.x[pix_ind])
            
        pix_prediction = params.light_amp[cam_ind]*z_y[pix_ind] 
#         tot_loglik += logpdf(Normal(pix_prediction + params.pedestal[cam_ind], params.light_fluct[cam_ind]*sqrt(pix_prediction)), image.y[pix_ind]) 
        tot_loglik += logpdf(Normal(pix_prediction + params.pedestal[cam_ind], sqrt(params.light_fluct[cam_ind]^2*pix_prediction + (params.bckgr_fluct[cam_ind])^2) ), image.y[pix_ind]) 
   
    end
    
    return tot_loglik
end

function generate_cam(
        params::NamedTuple, 
        image::NamedTuple,
        cam_ind::Integer
    ) where {F <: AbstractFloat}
    
    VT = eltype(params.tr_size_core)
      
    δ_x::VT = params.psx[cam_ind] * 10^-3
    δ_y::VT = params.psy[cam_ind] * 10^-3
    
    @inbounds μ_x::VT  = params.algmx[cam_ind] * δ_x
    @inbounds μ_y::VT  = params.algmy[cam_ind] * δ_y
    
    @inbounds σ_x_core::VT = sqrt.(params.tr_size_core[1]^2 + 10^-4*params.ang_spr_core[1]^2*(params.waist_core[1] - params.s_cam[cam_ind])^2) 
    @inbounds σ_y_core::VT = sqrt.(params.tr_size_core[2]^2 + 10^-4*params.ang_spr_core[2]^2*(params.waist_core[1] - params.s_cam[cam_ind])^2) 
    
    σ_x_core_res::VT = sqrt(σ_x_core^2 + (params.resx[cam_ind]*δ_x)^2)
    σ_y_core_res::VT = sqrt(σ_y_core^2 + (params.resy[cam_ind]*δ_y)^2) 
    
    @inbounds σ_x_halo::VT = sqrt.(params.tr_size_halo[1]^2 + 10^-4*params.ang_spr_halo[1]^2*(params.waist_halo[1] - params.s_cam[cam_ind])^2) 
    @inbounds σ_y_halo::VT = sqrt.(params.tr_size_halo[2]^2 + 10^-4*params.ang_spr_halo[2]^2*(params.waist_halo[1] - params.s_cam[cam_ind])^2) 
    
    σ_x_halo_res::VT = sqrt(σ_x_halo^2 + (params.resx[cam_ind]*δ_x)^2)
    σ_y_halo_res::VT = sqrt(σ_y_halo^2 + (params.resy[cam_ind]*δ_y)^2) 
    
    dist_core_x = Normal(μ_x, σ_x_core_res)
    dist_core_y = Normal(μ_y, σ_y_core_res)
    
    dist_halo_x = Normal(μ_x, σ_x_halo_res)
    dist_halo_y = Normal(μ_y, σ_y_halo_res)
    
    x_edges = range(0, length = length(image.x)+1, step=δ_x) 
    y_edges = range(0, length = length(image.y)+1, step=δ_y) 
    
    z_x = (params.mixt_pow.* diff(cdf.(dist_core_x, x_edges))) .+ ((1-params.mixt_pow).*diff(cdf.(dist_halo_x, x_edges)))
    z_y = (params.mixt_pow.* diff(cdf.(dist_core_y, y_edges))) .+ ((1-params.mixt_pow).*diff(cdf.(dist_halo_y, y_edges)))
    
    z_x = params.light_amp[cam_ind].*z_x .+ params.pedestal[cam_ind]
    z_y = params.light_amp[cam_ind].*z_y .+ params.pedestal[cam_ind]
    
    return (x=z_x, y=z_y)
end

function log_lik_ndiff(; d = data,n_threads = Threads.nthreads(),)
    
    nt_keys = collect(keys(d))
    cams_active = [!prod(isnan.(getindex(d, key).x)) for key in nt_keys]
    
    return params -> begin 
        
        ll = 0.0
        
        for (ind, vals) in enumerate(nt_keys[cams_active])
            ll += likelihood_cam(params, getindex(d, vals), ind, n_threads=n_threads)
        end
        
        return LogDVal(ll)
    end
        
end


function generate_event(params, d)
    
    nt_keys = collect(keys(d))   
    projected_data = NamedTuple()
    
    act_ind = 0
    for (ind, vals) in enumerate(nt_keys[1:8])
        
        data_cam = getindex(d, vals)
        
        if !prod(isnan.(data_cam.x))
            act_ind += 1
            cam_proj = generate_cam(params, data_cam, act_ind)
            cam_proj = (vals => cam_proj,)
            projected_data = merge(projected_data, cam_proj)
        else 
            cam_proj = (vals => (x=NaN, y=NaN),)
            projected_data = merge(projected_data, cam_proj)
        end
        
    end
    
    return projected_data
end


function def_prior(data)
    
    px_horizontal = [27.1, 21.6, NaN, 114.0, 44.3, 33.8, 40.3, 49.4] # add pixel size for BTV54
    px_vertical = [30.5, 23.4, NaN,  125.0, 43.9, 33.3, 39.7, 47.3]  # add pixel size for BTV54
    
    res_horizontal = [1., 1., 3., 1., 3., 3, 3, 3] 
    res_vertical = [1., 1., 3., 1., 3., 3, 3, 3]
    
    cam_positions = [0.0, 1.47799, NaN, 15.025999, 15.025999, 15.025999, 23.1644, 23.1644] # add position for BTV54
    
    nt_keys = collect(keys(data))
    cam_names = ["BTV50", "BTV53", "BTV54", "BTV26", "IS1 Core", "IS1 Halo", "IS2 Core", "IS2 Halo"]
    cams_active = [!prod(isnan.(getindex(data, key).x)) for key in nt_keys]
    cams_active_ind = Int64.(sort(findall(x -> x == true, cams_active)))
    
    function def_prior(dt; α=5)
        mean_pr = argmax(dt)
        npoints = length(dt)
        Uniform(mean_pr - npoints/α, mean_pr + npoints/α)
    end
    
    return NamedTupleDist(
        tr_size_core = [truncated(Normal(0.2, 0.04), 0.03, 0.3), truncated(Normal(0.2, 0.04), 0.03, 0.3)],
        tr_size_halo = [truncated(Normal(0.2, 0.04), 0.03, 0.3), truncated(Normal(0.2, 0.04), 0.03, 0.3)],
        ang_spr_core = [truncated(Normal(4.0, 2.0), 1, 4.0), truncated(Normal(4.0, 2.0), 1, 4.0)],
        ang_spr_halo = [truncated(Normal(4.0, 2.0), 4.5, 8.0), truncated(Normal(4.0, 2.0), 4.5, 8.0)],
        mixt_pow =  0 .. 1, #core
        waist_core = [truncated(Normal(2.774, 0.03), 2.5, 3.6)],
        waist_halo = [truncated(Normal(2.774, 0.03), 2.5, 3.6)], #11
        algmx = [def_prior(getindex(data, i).x) for i in nt_keys[cams_active]], # 12-15
        algmy = [def_prior(getindex(data, i).y) for i in nt_keys[cams_active]], #16-19
        pedestal = [Uniform(0, 0.4) for i in nt_keys[cams_active]], #20-23
        light_fluct = [Uniform(0, 0.2) for i in nt_keys[cams_active]], #24-27
        bckgr_fluct = [Uniform(0, 0.2) for i in nt_keys[cams_active]], #28-31
        light_amp = [Uniform(0, 80) for i in nt_keys[cams_active]], #31-35
        resx = [res_horizontal[i] for i in cams_active_ind], 
        resy = [res_vertical[i] for i in cams_active_ind], 
        psx = [px_horizontal[i] for i in cams_active_ind], 
        psy = [px_vertical[i] for i in cams_active_ind], 
        s_cam = [cam_positions[i] for i in cams_active_ind],
    )
    
    
end

function def_prior_1g(data)
    
    px_horizontal = [27.1, 21.6, NaN, 114.0, 44.3, 33.8, 40.3, 49.4]
    px_vertical = [30.5, 23.4, NaN, 125.0, 43.9, 33.3, 39.7, 47.3]
    
    res_horizontal = [1., 1., 3., 1., 3., 3, 3, 3]
    res_vertical = [1., 1., 3., 1., 3., 3, 3, 3]
    
    cam_positions = [0.0, 1.47799, 1.920999, 15.025999, 15.025999, 15.025999, 23.1644, 23.1644] 
    
    nt_keys = collect(keys(data))
    cam_names = ["BTV50", "BTV53", "BTV54", "BTV26", "IS1 Core", "IS1 Halo", "IS2 Core", "IS2 Halo"]
    cams_active = [!prod(isnan.(getindex(data, key).x)) for key in nt_keys]
    cams_active_ind = Int64.(sort(findall(x -> x == true, cams_active)))
    
    function def_prior(dt; α=5)
        mean_pr = argmax(dt)
        npoints = length(dt)
        Uniform(mean_pr - npoints/α, mean_pr + npoints/α)
    end
    
    return NamedTupleDist(
        tr_size_core = [truncated(Normal(0.2, 0.04), 0.05, 0.25), truncated(Normal(0.2, 0.04), 0.05, 0.25)],
        ang_spr_core = [truncated(Normal(4.0, 2.0), 2.5, 6.5), truncated(Normal(4.0, 2.0), 2.5, 6.5)],
        waist_core = [truncated(Normal(2.774, 0.03), 2.5, 3.6)],
        tr_size_halo = [0.15, 0.15],
        ang_spr_halo = [6.0, 6.0],
        mixt_pow = 1.0, #core
        waist_halo = [2.8], #11
        algmx = [def_prior(getindex(data, i).x) for i in nt_keys[cams_active]], # 12-15
        algmy = [def_prior(getindex(data, i).y) for i in nt_keys[cams_active]], #16-19
        pedestal = [Uniform(0, 0.15) for i in nt_keys[cams_active]], #20-23
        light_fluct = [Uniform(0, 0.2) for i in nt_keys[cams_active]], #24-27
        bckgr_fluct = [Uniform(0, 0.1) for i in nt_keys[cams_active]], #28-31
        light_amp = [Uniform(0, 80) for i in nt_keys[cams_active]], #31-35
        resx = [res_horizontal[i] for i in cams_active_ind], 
        resy = [res_vertical[i] for i in cams_active_ind], 
        psx = [px_horizontal[i] for i in cams_active_ind], 
        psy = [px_vertical[i] for i in cams_active_ind], 
        s_cam = [cam_positions[i] for i in cams_active_ind],
    )
    
    
end

function find_mode_event(edata, start_params, prior_func)
    pr = prior_func(edata)
    ll = log_lik_ndiff(d=edata);
    posterior = PosteriorDensity(ll, pr)
    ores = bat_findmode(posterior, MaxDensityNelderMead(init = ExplicitInit([start_params])))
    return ores.result
end